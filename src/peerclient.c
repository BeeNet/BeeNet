#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>

#include "beenetwork.h"

typedef struct clientRequestInfo {
	Settings *settings;
	ThreadLog *threadLog;
} clientRequestInfo;

THREAD_RETURN_T peerclient_request(void *vargp) {
	clientRequestInfo *requestInfo = (clientRequestInfo *) vargp;
	ThreadLog *threadLog = requestInfo->threadLog;

	struct sockaddr_in server_address = {0};
	int port = 2070;

	int sock = 0;
	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		logError("PeerClient", "Opening socket (%d: %s)", errno, strerror(errno));
		printf("\n");
		exit(1);
	}

	//memset(&server_address, '0', sizeof(server_address));
	server_address.sin_family = AF_INET;
	server_address.sin_port = htons(port);
	server_address.sin_addr.s_addr = inet_addr("127.0.0.1");

	// Convert IPv4 and IPv6 addresses from text to binary
	if (inet_pton(AF_INET, "127.0.0.1", &server_address.sin_addr) <= 0) {
		logError("PeerClient", "Invalid address");
		printf("\n");
		close(sock);
		exit(1);
	}

	// Connect to server
	if (connect(sock, (struct sockaddr *) &server_address, sizeof(server_address)) < 0) {
		logError("PeerClient", "Connection failed (%d: %s)", errno, strerror(errno));
		printf("\n");
		close(sock);
		exit(1);
	}

	// Send Echo 'Hello!' request
	char *str = "Hello!";
	size_t str_size = 7;
	if (protocol_client_sendEcho(sock, threadLog, str, str_size) < 0) {
		logError("PeerClient", "Writing to socket (%d: %s)", errno, strerror(errno));
		printf("\n");
		close(sock);
		exit(1);
	}

	// Wait for and Read response
	ProtocolServerCommand serverCommand = protocol_client_responseCommand(sock, threadLog);

	switch(serverCommand) {
		case ServerCommand_PONG:
		{
			logging_addDebugToThreadLog(threadLog, "Received pong.");
		} break;
		case ServerCommand_ECHO:
		{
			free(protocol_client_receiveEcho(sock, threadLog));
		} break;
	}

	freeThreadLog(threadLog);
	close(sock);
	// TODO(Christian): What's the difference between pthread_exit vs. just
	// returning from the function?
	pthread_exit(NULL);
}

THREAD_RETURN_T peerclient_first(void *vargp) {
	Settings *settings = (Settings *) vargp;

	ThreadLog *threadLog = createThreadLog(settings, MUT("PeerClient"));

	logging_addInfoToThreadLog(threadLog, "Started thread.");

	pthread_t tid[55];
	for (int i = 0; i < 10; i++) {
		ThreadLog *threadLog = createThreadLog(settings, MUT("PeerClient"));

		clientRequestInfo *requestInfo = malloc(sizeof(clientRequestInfo) * 1);
		requestInfo->settings = settings;
		requestInfo->threadLog = threadLog;

		if (pthread_create(&tid[i], NULL, peerclient_request, (void *) requestInfo) != 0) {
			logError("PeerClient", "Failed to create thread");
		}
		++i;
	}

	for (int i = 0; i < 10; i++) {
		pthread_join(tid[i], NULL);
	}

	sleep(4);

	for (int i = 0; i < 10; i++) {
		ThreadLog *threadLog = createThreadLog(settings, MUT("PeerClient"));

		clientRequestInfo *requestInfo = malloc(sizeof(clientRequestInfo) * 1);
		requestInfo->settings = settings;
		requestInfo->threadLog = threadLog;

		if (pthread_create(&tid[i], NULL, peerclient_request, (void *) requestInfo) != 0) {
			logError("PeerClient", "Failed to create thread");
		}
	}

	for (int i = 0; i < 10; i++) {
		pthread_join(tid[i++], NULL);
	}

	logging_addInfoToThreadLog(threadLog, "Closing.");

#ifndef _WIN32
	return NULL;
#endif
}