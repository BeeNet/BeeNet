#include "beenetwork.h"

// Gives back pointer starting at next non-whitespace character that appears after start
char *skipWhitespace(char *start) {
	char *current = start;
	if (*current == '\0') return current;

	while (*current == ' ' || *current == '\t' || *current == '\n' || *current == '\r') {
		++current;
		if (*current == '\0') break;
	}

	return current;
}

// Will give back a pointer just after the word skipped.
char *skipWord(char *start, bool includeNumbers, bool includeSymbols) {
	char *current = start;
	while ((*current >= 'A' && *current <= 'Z') || (*current >= 'a' && *current <= 'z')
	    || (includeNumbers && *current >= '0' && *current <= '9')
	    || (includeNumbers && *current == '-') // Note: Technically this will also match numbers that have '-' in the middle and at end, but whatever TODO: Fix this
	    || (includeSymbols && *current >= '!' && *current <= '/')
	    || (includeSymbols && *current >= ':' && *current <= '@')
	    || (includeSymbols && *current >= '[' && *current <= '`')
	    || (includeSymbols && *current >= '{' && *current <= '~')) {

		++current;

		if (*current == '\0') break;
	}
	return current;
}

// TODO
char *skipText() {
	return NULL;
}

// TODO
char *skipSymbols() {
	return NULL;
}

char *skipNumbers(char *start) {
	char *current = start;
	if (*current == 0) return current;

	if (*current == '-') ++current; // Note: Not doing '+' here
	if (*current == 0) return current;

	while (*current >= '0' && *current <= '9') {
		++current;
		if (*current == 0) break;
	}
	return current;
}

/* Gets input, trims leading space, and puts into line char array
as long as it doesn't go over the max.

@line - array of characters to fill up
@max - maximum amount of characters allowed in line
@trimSpace - Whether to trim leading space (1) or not (0)
@return - the length of the line
*/
int parsing_getLine(char *line, int max, int trimSpace) {
	int c;
	int i = 0;

	/* Trim whitespace */
	while (trimSpace && ((c = getchar()) == ' ' || c == '\t'))
		;

	if (!trimSpace) c = getchar();

	/* If there's nothing left, return */
	if (c == '\n') {
		line[i] = '\0';
		return 1; /* Including \0 */
	}

	/* Transfer input characters into line string */
	while (c != EOF && c != '\n' && i < max)
	{
		line[i] = (char) c;
		++i;
		c = getchar();
	}

	/* End of string */
	line[i] = '\0';
	++i; /* Includes '\0' in the length */

	return i;
}

// atoi with provided length
int antoi(const char* str, int len)
{
    int i;
    int ret = 0;
    for(i = 0; i < len; ++i)
    {
        ret = ret * 10 + (str[i] - '0');
    }
    return ret;
}

void parseArgs(char *args_string, Settings *settings) {
	char *current = args_string;

	// Loop through all characters
	while (*current) {
		current = skipWhitespace(current);
		if (*current == '\0') return;

		// Get whether argument is short vs. long argument ('-' vs. '--')
		bool shortArg = false;
		bool longArg = false;
		if (*current == '-') {
			shortArg = true;
			++current;
			if (*current == '\0') return;
		}
		if (*current == '-') {
			shortArg = false;
			longArg = true;
			++current;
			if (*current == '\0') return;
		}

		// Argument not prefixed with '-' or '--', then skip
		if (!shortArg && !longArg) {
			current = skipWord(current, false, false);
			if (*current == '\0') return;
			++current;
			continue;
		}

		// Get argument's key
		pString argKey = {0};
		argKey.start = current;
		current = skipWord(current, false, false);
		argKey.end = current;
		int argKeyLength = argKey.end - argKey.start;
		//printf("%.*s\n", argKeyLength, argKey.start);

		// Make sure '=' sign is used for long arguments
		if (longArg && *current == '=') {
			++current;
			if (*current == '\0') return;

			// Get value
			pString argValue = {0};
			argValue.start = current;
			current = skipWord(current, true, true);
			argValue.end = current;
			int argValueLength = argValue.end - argValue.start;

			// Set settings
			if (strncmp(argKey.start, "port", MAX(4, argKeyLength)) == 0) {
				settings->peerserver_port = antoi(argValue.start, argValueLength);
			}
			++current;
		} else if (shortArg) {
			// If short argument, value is separated with whitespace
			current = skipWhitespace(current);
			if (*current == '\0') return;

			// Get value
			pString argValue = {0};
			argValue.start = current;
			current = skipWord(current, true, true);
			argValue.end = current;
			int argValueLength = argValue.end - argValue.start;

			// Set settings
			if (strncmp(argKey.start, "p", MAX(1, argKeyLength)) == 0) {
				settings->peerserver_port = antoi(argValue.start, argValueLength);
			}
			++current;
		} else {
			// TODO: Set value to true if boolean argument
		}
	}
}