#include "beenetwork.h"
#include <inttypes.h>

#ifdef Jemalloc
#include <jemalloc/jemalloc.h>
#endif

// Create threadlog, returning the pointer to the threadlog
// logNamespace should be a c-string // @TODO
ThreadLog* createThreadLog(Settings *settings, char *logNamespace) {
    ThreadLog *threadLog = xmalloc(sizeof(ThreadLog));
    threadLog->destroyed = false;
    threadLog->logNamespace = logNamespace;
    threadLog->logStrings = NULL;
    threadLog->logTypes = NULL;

    pthread_mutex_lock(&settings->lock);
    {
        buf_push(settings->threadLogs, threadLog);
        pthread_mutex_init(&threadLog->lock, NULL);
    }
    pthread_mutex_unlock(&settings->lock);

    return threadLog;
}

// @NOTE(Christian): We need to make sure the logging thread is able to print
// the strings before the threadlog is freed - so the logging thread should probably
// do the threadLog cleanup so that it can print off the logs first.
void freeThreadLog(ThreadLog *threadLog) {
    threadLog->destroyed = true;
}

void logging_addToThreadLog(ThreadLog* threadLog, char *stringBuffer, LOG_TYPE logType) {
    //logDebug("Logging", "Adding a string. Already Existing Length: %d", buf_len(threadLog->logStrings));
    pthread_mutex_lock(&threadLog->lock);
    {
        buf_push(threadLog->logStrings, stringBuffer);
        buf_push(threadLog->logTypes, logType);
    }
    pthread_mutex_unlock(&threadLog->lock);
}

bool isSpecial(char c) {
#ifdef _WIN32
    if (c == INPUT_SPECIAL1 || c == INPUT_SPECIAL2) {
        return true;
    }
#else
    if (c == INPUT_SPECIAL1) {
        char next = getch();
        if (next == INPUT_SPECIAL2) {
            return true;
        }
    }
#endif

    return false;
}

#ifdef DEBUG
void addDebugCommand(Map *debugCommands, const char *name, debugCommandFunc function) {
    uint64_t hash = hash_bytes(name, strlen(name) * sizeof(char));
    logDebug("Logging", "Add Debug Command: '%s', %" PRIu64 ", %p", name, hash, function);
    map_put_from_uint64(debugCommands, hash, (void *) function);

    //debugCommandFunc get_function = (debugCommandFunc) map_get_from_uint64(debugCommands, hash);
    //logDebug("Logging", "Test Get Debug Command: '%s', %p == %p", name, function, get_function);
}

// Add commands hashmap with function pointers
void addDebugCommands(Map *debugCommands) {
    addDebugCommand(debugCommands, "info", debugCommand_info);
    addDebugCommand(debugCommands, "threadlogs", debugCommand_threadlogs);

    addDebugCommand(debugCommands, "pause_logging", debugCommand_pause_logging);
    addDebugCommand(debugCommands, "start_logging", debugCommand_start_logging);
    addDebugCommand(debugCommands, "resume_logging", debugCommand_start_logging);

#ifdef Jemalloc
    addDebugCommand(debugCommands, "jemalloc_stats", debugCommand_jemalloc_stats);
#endif

    addDebugCommand(debugCommands, "clear", debugCommand_clear);
    addDebugCommand(debugCommands, "exit", debugCommand_exit);

    addDebugCommand(debugCommands, "commands", debugCommand_commands);
}
#endif

THREAD_RETURN_T logging_first(void *vargp) {
    Settings *settings = (Settings *) vargp;
    Map debugCommands = {0};

#ifdef DEBUG
    addDebugCommands(&debugCommands);
#endif

    bool reprintInput = true;
    bool doLogging = true;
    char *input = NULL;
    char **input_history = NULL;
    int history_index = 0; // 0 == none, 1 == buf_len(input_history) - 1

    while (true) {
        // Go through all thread logs to check if logging is set to true
        if (doLogging && pthread_mutex_trylock(&settings->lock) == 0) {
            ThreadLog **newThreadLogs = NULL;

            for (int i = 0; i < buf_len(settings->threadLogs); i++) {
                ThreadLog *threadLog = settings->threadLogs[i];

                if (pthread_mutex_trylock(&threadLog->lock) == 0) {
                    char **logStrings = threadLog->logStrings;
                    LOG_TYPE *logTypes = threadLog->logTypes;
                    threadLog->logStrings = NULL;
                    threadLog->logTypes = NULL;

                    // Log all strings
                    for (int x = 0; x < buf_len(logStrings); x++) {
                        reprintInput = true;
                        putchar('\r');
                        switch(logTypes[x]) {
                            case LOG_ERROR:
                            {
                                printError("[ERROR:%s] ", threadLog->logNamespace);
                                printf("%.*s\n", (int) buf_len(logStrings[x]), logStrings[x]);
                            } break;
                            case LOG_DEBUG:
                            {
                                printDebug("[DEBUG:%s] ", threadLog->logNamespace);
                                printf("%.*s\n", (int) buf_len(logStrings[x]), logStrings[x]);
                            } break;
                            case LOG_INFO:
                            {
                                printInfo("[INFO:%s] ", threadLog->logNamespace);
                                printf("%.*s\n", (int) buf_len(logStrings[x]), logStrings[x]);
                            } break;
                        }
                        buf_free(logStrings[x]);
                    }

                    buf_pop_all(threadLog->logStrings);
                    buf_pop_all(threadLog->logTypes);

                    // Destroy the threadLog
                    if (threadLog->destroyed) {
                        free(threadLog->logNamespace);
                        buf_free(threadLog->logTypes);

                        for (int s_index = 0; s_index < buf_len(threadLog->logStrings); s_index++) {
                            buf_free(threadLog->logStrings[s_index]);
                        }

                        buf_free(threadLog->logStrings);
                        pthread_mutex_destroy(&threadLog->lock);

                        free(threadLog);
                        continue;
                    }

                    // Leave in threadLogsArray
                    buf_push(newThreadLogs, threadLog);

                    pthread_mutex_unlock(&threadLog->lock);
                } else {
                    // Leave in threadLogsArray
                    buf_push(newThreadLogs, threadLog);
                }
            }

            buf_free(settings->threadLogs);
            settings->threadLogs = newThreadLogs;

            pthread_mutex_unlock(&settings->lock);
        }

#ifdef DEBUG
        char c;
        if ((c = getch_nonblocking())) { // @TODO: getch_nonblocking is unix-only right now
            if (c == INPUT_BACKSPACE) {
                if (buf_len(input) > 0) {
                    (void)buf_pop(input);
                    if (!reprintInput) {
                        printf("\b \b");
                    }
                }
            } else if (c == '\n') {
                logging_executeCommand(&debugCommands, input, settings, &doLogging);
                buf_push(input_history, input);
                input = NULL;
                history_index = 0;
                reprintInput = true;
            } else if (isSpecial(c)) {
                c = getch();
                bool upOrDown = false;

                if (c == INPUT_UP) {
                    upOrDown = true;
                    if (history_index < buf_len(input_history))
                        ++history_index;
                } else if (c == INPUT_DOWN) {
                    upOrDown = true;
                    if (history_index != 0)
                        --history_index;
                }

                if (upOrDown) {
                    int spaces_count = buf_len(input);
                    if (history_index > 0 && history_index <= buf_len(input_history)) {
                        buf_pop_all(input);
                        char *hist_buf = input_history[buf_len(input_history) - history_index];
                        for (int i = 0; i < buf_len(hist_buf); i++) {
                            buf_push(input, hist_buf[i]);
                        }
                    } else if (history_index <= 0) {
                        buf_pop_all(input);
                    }

                    // Print spaces to clear the word from the terminal
                    for (int i = 0; i < spaces_count; i++) {
                        fputs("\b \b", stdout);
                    }
                    reprintInput = true;
                }
            } else if (c >= 32 && c <= 126) {
                buf_push(input, c);
                if (!reprintInput) {
                    printf("%c", c);
                }
            }
        }

        if (reprintInput) {
            putchar('\r');
            printPrompt("DEBUG > ");
            printf("%.*s", (int)buf_len(input), input);

            reprintInput = false;
        }
#endif

    }
}

#ifdef DEBUG
void logging_executeCommand(Map *debugCommands, char *input, Settings *settings, bool *doLogging) {
    char *current = input;
    char *endBound = buf_end(input);

    if (buf_len(input) == 0) {
        // strncmp doesn't work well with null pointers even if size is zero. We
        // actually handle another case in this if: the command might be empty
        printf("\n");
        return;
    }

    pString command;

    // Skip whitespace @TODO: Switch to my parsing.c version once I get that back in here
    while (current < endBound && (*current == ' ' || *current == '\t')) ++current;

    command.start = current;
    while (current < endBound && *current != ' ' && *current != '\n' && *current != '\r') {
        ++current;
    }
    command.end = current;
    size_t command_length = command.end - command.start;

    uint64_t command_hash = hash_bytes(command.start, command_length * sizeof(char));
    //printf("\n%" PRIu64 "\n", command_hash);

    debugCommandFunc function = map_get_from_uint64(debugCommands, command_hash);
    if (function != NULL) {
        function(command.end, endBound, settings, doLogging);
        return;
    } else {
        printf("\n?\n");
    }
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
void debugCommand_info(char *args, char *endBound, Settings *settings, bool *doLogging) {
    printf("\n\nInfo:\n");
    printf("BeeNetwork - Created by Ivanq and Krixano\n");
    printf("Version: " version "\n");
    printf("Port: %d\n", settings->peerserver_port);
    printf("Ip Address: %s\n", settings->peerserver_ip);
    printf("Thread Logs Count: %d\n", (int)buf_len(settings->threadLogs));
    printf("\n\n");
}

void debugCommand_threadlogs(char *args, char *endBound, Settings *settings, bool *doLogging) {
    printf("\n\nThreadLogs:\n");
    pthread_mutex_lock(&settings->lock);
    {
        for (int i = 0; i < buf_len(settings->threadLogs); i++) {
            pthread_mutex_lock(&settings->threadLogs[i]->lock);
            {
                printf("* [%d] %s (%p)\n", i, settings->threadLogs[i]->logNamespace, settings->threadLogs[i]);
                printf("  - Count: %d\n", (int)buf_len(settings->threadLogs[i]->logStrings));
                if (settings->threadLogs[i]->destroyed)
                    printf("  - Destroyed\n");
            }
            pthread_mutex_unlock(&settings->threadLogs[i]->lock);
        }
    }
    pthread_mutex_unlock(&settings->lock);
    printf("\n");
}

void debugCommand_pause_logging(char *args, char *endBound, Settings *settings, bool *doLogging) {
    *doLogging = false;
    printf("\n");
}

void debugCommand_start_logging(char *args, char *endBound, Settings *settings, bool *doLogging) {
    *doLogging = true;
    printf("\n");
}

#ifdef Jemalloc
void debugCommand_jemalloc_stats(char *args, char *endBound, Settings *settings, bool *doLogging) {
    printf("\n\n");
    malloc_stats_print(NULL, NULL, NULL);
    printf("\n");
}
#endif

void debugCommand_clear(char *args, char *endBound, Settings *settings, bool *doLogging) {
    clrscr();
}

void debugCommand_exit(char *args, char *endBound, Settings *settings, bool *doLogging) {
    printf("\n");
    exit(1);
}

void debugCommand_commands(char *args, char *endBound, Settings *settings, bool *doLogging) {
    printf("\n\nCommands:\n");

    printf("* commands\n");
    printf("* info\n");
    printf("* threadlogs\n");
    printf("* pause_logging\n");
    printf("* start_logging\n");
    printf("* resume_logging\n");

#ifdef Jemalloc
    printf("* jemalloc_stats\n");
#endif

    printf("* clear\n");
    printf("* exit\n");

    printf("\n");
}
#endif

#pragma GCC diagnostic pop