#include "beenetwork.h"

void *buf__grow(const void *buf, size_t new_len, size_t elem_size) {
	assert(buf_cap(buf) <= (SIZE_MAX - 1) / 2);
	size_t new_cap = MAX(1 + 2 * buf_cap(buf), new_len);
	assert(new_len <= new_cap);
	assert(new_cap <= (SIZE_MAX - offsetof(BufHdr, buf)) / elem_size);
	size_t new_size = offsetof(BufHdr, buf) + new_cap * elem_size;

	BufHdr *new_hdr;
	if (buf) {
		new_hdr = xrealloc(buf__hdr(buf), new_size);
	} else {
		new_hdr = xmalloc(new_size);
		new_hdr->len = 0;
	}
	new_hdr->cap = new_cap;
	return new_hdr->buf;
}


void *xcalloc(size_t num_elems, size_t elem_size) {
	void *ptr = calloc(num_elems, elem_size);
	if (!ptr) {
		perror("xcalloc failed");
		exit(1);
	}
	return ptr;
}

void *xrealloc(void *ptr, size_t num_bytes) {
	ptr = realloc(ptr, num_bytes);
	if (!ptr) {
		perror("xrealloc failed");
		exit(1);
	}
	return ptr;
}

void *xmalloc(size_t num_bytes) {
	void *ptr = malloc(num_bytes);
	if (!ptr) {
		perror("xmalloc failed");
		exit(1);
	}
	return ptr;
}


void fatal(const char *fmt, ...) {
	va_list args;
	va_start(args, fmt);
	printf("FATAL: ");
	vprintf(fmt, args);
	printf("\n");
	va_end(args);
	exit(1);
}

// Copy buffer into new buffer
void *buf_copy(void *buf, size_t elem_size) {
	size_t size = offsetof(BufHdr, buf) + buf_cap(buf) * elem_size;

	BufHdr *new_hdr = xmalloc(size);
	new_hdr->len = buf_len(buf);
	new_hdr->cap = buf_cap(buf);

	memcpy(new_hdr->buf, buf, buf_len(buf) * elem_size);

	return new_hdr->buf;
}

// Creates a new char stretchy buffer from a c-string.
// Will allocate everything at once based on the size of the c-string.
char *cStrToBuffer(char const *str) {
	char *buf = NULL;
	size_t strLength = strlen(str);
	buf__fit(buf, strLength);

	for (int i = 0; i < strLength; i++) {
		buf[buf__hdr(buf)->len++] = str[i];
	}

	return buf;
}

char *strFormatToBuffer(char const *format, ...) {
	va_list args;
	va_start(args, format);

	char str[1024]; // @TODO
	vsprintf(str, format, args);
	va_end(args);

	char *buf = NULL;
	size_t strLength = strlen(str);
	buf__fit(buf, strLength);

	for (int i = 0; i < strLength; i++) {
		buf[buf__hdr(buf)->len++] = str[i];
	}

	return buf;
}

// Creates a new char stretchy buffer from a string with a given size
// Will allocate everything at once based on the give size of the string.
char *strToBuffer(char const *str, size_t strLength) {
	char *buf = NULL;
	buf__fit(buf, strLength);

	for (int i = 0; i < strLength; i++) {
		buf[buf__hdr(buf)->len++] = str[i];
	}

	return buf;
}
