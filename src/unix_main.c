#include <stdio.h>

#include "beenetwork.h"

char getch() {
	char buf=0;
	struct termios old={0};
	fflush(stdout);
	if(tcgetattr(0, &old)<0)
		perror("tcsetattr()");
	old.c_lflag&=~ICANON;
	old.c_lflag&=~ECHO;
	old.c_cc[VMIN]=1;
	old.c_cc[VTIME]=0;
	if(tcsetattr(0, TCSANOW, &old)<0)
		perror("tcsetattr ICANON");
	if(read(0,&buf,1)<0)
		perror("read()");
	old.c_lflag|=ICANON;
	old.c_lflag|=ECHO;
	if(tcsetattr(0, TCSADRAIN, &old)<0)
		perror ("tcsetattr ~ICANON");
	return buf;
}

char getch_nonblocking() {
	/*#include <unistd.h>   //_getch*/
	/*#include <termios.h>  //_getch*/
	char buf=0;
	struct termios old={0};
	fflush(stdout);
	if(tcgetattr(0, &old)<0)
		perror("tcsetattr()");
	old.c_lflag&=~ICANON;
	old.c_lflag&=~ECHO;
	old.c_cc[VMIN]=0;
	old.c_cc[VTIME]=0;
	if(tcsetattr(0, TCSANOW, &old)<0)
		perror("tcsetattr ICANON");
	if(read(0,&buf,1)<0)
		perror("read()");
	old.c_lflag|=ICANON;
	old.c_lflag|=ECHO;
	if(tcsetattr(0, TCSADRAIN, &old)<0)
		perror ("tcsetattr ~ICANON");
	return buf;
}

void clrscr() {
	printf("\e[1;1H\e[2J"); // TODO: Check whether actually portable
}

TermSize getTermSize() {
	struct winsize w;
	ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);

	TermSize size = { w.ws_row, w.ws_col };
	return size;
}

Settings defaultSettings() {
	Settings settings = {0};
	settings.peerserver_port = 2070;
	settings.peerserver_ip = NULL; // Any address
	settings.threadLogs = NULL;

	// Initialize mutex lock
	if (pthread_mutex_init(&settings.lock, NULL) != 0) {
		logError("", "Failed to create settings mutex lock");
	}

	return settings;
}

int main(int argc, char **argv) {
	printf("Welcome to the BeeNetwork.\n");
	printf("Created by Imachug and Krixano.\n");
	printf("\n");

	Settings settings = defaultSettings();

	// Combine all arguments into one string
	int total_args_length = 0;
	for (int i = 1; i < argc; i++) {
		// Add one for the space that separates each argument
		total_args_length += strlen(argv[i]) + 1;
	}
	if(total_args_length == 0) {
		total_args_length = 1; // empty argument list
	}
	char *args_string = xmalloc(total_args_length);
	char *args_p = args_string;
	for (int i = 1; i < argc; i++) {
		memcpy(args_p, argv[i], strlen(argv[i]));
		args_p += strlen(args_p);
		*args_p++ = ' ';
	}
	// Go backward 1 to overwrite the space placed after the last argument
	if(args_p > args_string) {
		args_p--;
	}
	*args_p = '\0';

	logDebug("Args", "'%s'", args_string);

	parseArgs(args_string, &settings);

	pthread_t thread_peerserver;
	pthread_t thread_peerclient;
	pthread_t thread_logging;
	pthread_create(&thread_peerserver, NULL, peerserver_first, (void *) &settings);
	pthread_create(&thread_peerclient, NULL, peerclient_first, (void *) &settings);
	pthread_create(&thread_logging, NULL, logging_first, (void *) &settings);

	// @NOTE(Christian): For testing the hashmap
	/*{
		Map map = {0};
		char *str0 = cStrToBuffer("Testing Hashmap");
		map_put(&map, (void *) 1, (void *) str0);

		char *getstr = (char *) map_get(&map, (void *) 1);
		printf("%.*s\n", buf_len(getstr), getstr);
	}*/

	pthread_join(thread_peerserver, NULL);
	pthread_join(thread_peerclient, NULL);
	pthread_join(thread_logging, NULL);

	printf("\n");

	return 0;
}