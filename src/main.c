#include <stdio.h>

#include "beenetwork.h"

int main(void) {
	printf("Welcome to the BeeNetwork.\n");
	printf("Created by Imachug and Krixano.\n");
	printf("\n");
	
	pthread_t thread_fileserver;
	pthread_create(&thread_fileserver, NULL, fileserver_first, NULL);
	
	pthread_join(thread_fileserver, NULL);
	printf("Goodbye!\n");
	
	return(0);
}
