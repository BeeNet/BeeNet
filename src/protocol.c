#include "beenetwork.h"
#include <arpa/inet.h>
#include <netinet/in.h>

ssize_t protocol_client_sendPing(int socket, ThreadLog *threadLog) {
    logging_addDebugToThreadLog(threadLog, "Sending ping.");
    ProtocolClientCommand clientCommand = ClientCommand_PING;
    return write(socket, &clientCommand, sizeof(ProtocolClientCommand));
}

ssize_t protocol_client_sendEcho(int socket, ThreadLog *threadLog, char *str, Size_Field str_byte_size) {
    char logStr[255];
    sprintf(logStr, "Sending echo: '%.*s'", str_byte_size, str);
    logging_addDebugToThreadLog(threadLog, logStr);
    //logging_addToThreadLog(threadLog, cStrToBuffer(logStr), LOG_DEBUG);

    // Send command
    ProtocolClientCommand clientCommand;
    clientCommand = ClientCommand_ECHO;
    write(socket, &clientCommand, sizeof(ProtocolClientCommand));

    // Convert to network endianness
    Size_Field str_byte_size_network = Size_Field_hton(str_byte_size);
    // Send string size
    write(socket, &str_byte_size_network, sizeof(Size_Field));

    // @TODO: Convert string to network endianness
    // Send string
    return write(socket, str, str_byte_size);
}

ProtocolServerCommand protocol_client_responseCommand(int socket, ThreadLog *threadLog) {
    ProtocolServerCommand serverCommand;
    read(socket, &serverCommand, sizeof(ProtocolServerCommand));

    return serverCommand;
}

char *protocol_client_receiveEcho(int socket, ThreadLog *threadLog) {
    // Get string size, converting from network endianness to host endianness
    Size_Field str_byte_size;
    read(socket, &str_byte_size, sizeof(Size_Field));
    str_byte_size = Size_Field_ntoh(str_byte_size);

    char str[255];
    sprintf(str, "Received echo length: %d", str_byte_size);
    logging_addDebugToThreadLog(threadLog, str);

    // Get string
    char *buffer = malloc(str_byte_size);
    read(socket, buffer, str_byte_size);

    sprintf(str, "Received echo: '%.*s'", str_byte_size, buffer);
    logging_addDebugToThreadLog(threadLog, str);

    return buffer;
}


ProtocolClientCommand protocol_server_requestCommand(int socket, serverRequestInfo *requestInfo) {
    ProtocolClientCommand clientCommand;
    read(socket, &clientCommand, sizeof(ProtocolClientCommand));
    return clientCommand;
}

ssize_t protocol_server_ping(int socket, serverRequestInfo *requestInfo) {
    logging_addDebugToThreadLog(requestInfo->threadLog, "Received ping. Sending pong.");
    ProtocolServerCommand serverCommand = ServerCommand_PONG;
    return write(socket, &serverCommand, sizeof(ProtocolServerCommand));
}

char *protocol_server_echo(int socket, serverRequestInfo *requestInfo) {
    // Get size of string, converting from network endianness to host endianness
    Size_Field str_byte_size;
    read(socket, &str_byte_size, sizeof(Size_Field));
    str_byte_size = Size_Field_ntoh(str_byte_size);

    char str[255];
    sprintf(str, "Sending echo length: %d", str_byte_size);
    logging_addDebugToThreadLog(requestInfo->threadLog, str);

    // Get string
    char *buffer = malloc(str_byte_size);
    read(socket, buffer, str_byte_size);

    sprintf(str, "Sending echo: '%.*s'", str_byte_size, buffer);
    logging_addDebugToThreadLog(requestInfo->threadLog, str);

    // Send the echo command back
    ProtocolServerCommand serverCommand = ServerCommand_ECHO;
    write(socket, &serverCommand, sizeof(ProtocolServerCommand));

    // Convert string size to network endianness
    Size_Field str_byte_size_network = Size_Field_hton(str_byte_size);
    write(socket, &str_byte_size_network, sizeof(Size_Field));

    // Send string
    write(socket, buffer, str_byte_size);

    return buffer;
}
