#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <process.h>

#include "beenetwork.h"

// Hack for clearing screen for Windows // TODO: Improve this
void clrscr() {
    system("cls");
}

TermSize getTermSize() {
	// TODO
	return { 0, 0 };
}

Settings defaultSettings() {
	Settings settings = {0};
	settings.peerserver_port = 2070;
	settings.peerserver_ip = NULL;
	settings.threadLogs = NULL;

	return settings;
}

int main(int argc, char *argv[]) {
	printf("Welcome to the BeeNetwork.\n");
	printf("Created by Imachug and Krixano.\n");
	printf("\n");

	Settings settings = defaultSettings();

	// Combine all arguments into one string
	int total_args_length = 0;
	for (int i = 1; i < argc; i++) {
		total_args_length += strlen(argv[i]) + 1;
	}
	char *args_string = xmalloc(total_args_length);
	char *args_p = args_string;
	for (int i = 1; i < argc; i++) {
		memcpy(args_p, argv[i], strlen(argv[i]));
		args_p += strlen(args_p);
		*args_p++ = ' ';
	}
	*--args_p = '\0';

	logDebug("Args", "'%s'", args_string);

	parseArgs(args_string, &settings);

	int threadCount = 0;

	++threadCount;
	_beginthread(peerserver_first, 0, &threadCount);

	printf("\n");

	return 0;
}