#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>

#include "beenetwork.h"

#define serverMaxConcurrentRequests 50

// @Performance: We're allocating memory for every single request being accepted
// Instead, we can have a pool of memory that's pre-allocated for the max amount
// of concurrent requests.
serverRequestInfo *createServerRequestInfo(Settings *settings, int requestId, int socket) {
	serverRequestInfo *requestInfo;
	requestInfo = malloc(sizeof(serverRequestInfo) * 1);

	requestInfo->requestId = requestId;
	requestInfo->socket = socket;
	requestInfo->settings = settings;

	// Create the threadlog // @TODO(Christian)
	char *logNamespace = malloc(sizeof("PeerServer:") + sizeof(requestId) * 8 / 3 + 2); // a good upper bound
	sprintf(logNamespace, "PeerServer:%d", requestId);
	requestInfo->threadLog = createThreadLog(settings, logNamespace);

	return requestInfo;
}

THREAD_RETURN_T peerserver_request(void *arg) {
	serverRequestInfo *requestInfo = arg;
	int requestId = requestInfo->requestId;
	int newSocket = requestInfo->socket;

	logging_addInfoToThreadLog(requestInfo->threadLog, "Started thread.");

	// Wait for and read request
	ProtocolClientCommand clientCommand = protocol_server_requestCommand(newSocket, requestInfo);

	int n = 0;
	switch(clientCommand) {
		case ClientCommand_PING:
		{
			n = protocol_server_ping(newSocket, requestInfo);
		} break;
		case ClientCommand_ECHO:
		{
			free(protocol_server_echo(newSocket, requestInfo));
		}
	}

	if (n < 0) {
		logError("PeerServer", "Request %d; Writing to socket (%d: %s)", requestId, errno, strerror(errno));
		printf("\n");
		close(newSocket);
		exit(1);
	}

	logging_addInfoToThreadLog(requestInfo->threadLog, "Closing.");
	close(newSocket);

	freeThreadLog(requestInfo->threadLog);
	free(requestInfo);

	return NULL;
}

THREAD_RETURN_T peerserver_first(void *vargp) {
	Settings *settings = (Settings *) vargp;
	struct sockaddr_storage serverStorage;

	ThreadLog* threadLog = createThreadLog(settings, MUT("PeerServer"));


	// @TODO(Christian): Using Unix/Posix sockets only

	int serverSocket, port;
	port = settings->peerserver_port;
	logging_addInfoToThreadLog(threadLog, "Started thread.");

	// @TODO(Christian): Blech!!!
	char str[255];
	sprintf(str, "Port: %d", port);
	logging_addInfoToThreadLog(threadLog, str);
	//logging_addToThreadLog(threadLog, cStrToBuffer(str), LOG_INFO);

	struct sockaddr_in server_address = {0};

	// Create the socket
	serverSocket = socket(AF_INET, SOCK_STREAM, 0);
	if (serverSocket < 0) {
		logError("PeerServer", "Opening socket (%d: %s)", errno, strerror(errno));
		printf("\n");
		exit(1);
	}

	// Forcefully attach socket to port
	int opt = 1;
	if (setsockopt(serverSocket, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
		logError("PeerServer", "Setsockopt");
		printf("\n");
		close(serverSocket);
		exit(1);
	}

	server_address.sin_family = AF_INET;
	server_address.sin_port = htons(port);

	// Set IP Address of server
	// inet_addr("127.0.0.1")
	if (settings->peerserver_ip == NULL) {
		server_address.sin_addr.s_addr = INADDR_ANY;
	} else {
		server_address.sin_addr.s_addr = inet_addr(settings->peerserver_ip);
	}

	// Bind address to socket
	if (bind(serverSocket, (struct sockaddr *) &server_address, sizeof(server_address)) < 0) {
		logError("PeerServer", "On binding (%d: %s)", errno, strerror(errno));
		printf("\n");
		close(serverSocket);
		exit(1);
	}

	// Listen on socket, with max connection requests queued
	if (listen(serverSocket, serverMaxConcurrentRequests) < 0) {
		logError("PeerServer", "Listen (%d: %s)", errno, strerror(errno));
		printf("\n");
		close(serverSocket);
		exit(1);
	} else {
		pthread_t tid[serverMaxConcurrentRequests + 10];
		int i = 0;

		int newSocket;
		while (true) { // @TODO(Christian): This is wrong, not decrementing i when thread finishes
			socklen_t address_size = sizeof serverStorage;

			// Wait for client connection, Create new socket for it.
			newSocket = accept(serverSocket, (struct sockaddr *) &serverStorage, &address_size);
			if (newSocket < 0) {
				logError("PeerServer", "On accept (%d: %s)", errno, strerror(errno));
				printf("\n");
				close(serverSocket);
				exit(1);
			}

			// For each client request, create new thread
			serverRequestInfo *requestInfo = createServerRequestInfo(settings, i, newSocket);
			if (pthread_create(&tid[i], NULL, peerserver_request, requestInfo) != 0) {
				logging_addErrorToThreadLog(threadLog, "Failed to create thread for request");
				//logging_addToThreadLog(threadLog, cStrToBuffer("Failed to create thread for request (" __FILE__ ")"), LOG_ERROR);
			} else {
				i++;
			}

			// If more than max active threads/requests, wait on all the active
			// threads/requests before continuing to accept more
			// @Performance(Christian): When we reach the max amount of connections, we wait for
			// *all* of them to finish. We should really only have to wait until there's
			// enough space for the next request
			if (i >= serverMaxConcurrentRequests) {
				sprintf(str, "Waiting for %d active connections to finish (%s:%u)", port, __FILE__, __LINE__);
				logging_addDebugToThreadLog(threadLog, str);
				//logging_addToThreadLog(threadLog, cStrToBuffer(str), LOG_DEBUG);

				i = 0;
				while (i < serverMaxConcurrentRequests) {
					pthread_join(tid[i++], NULL);
				}
				i = 0;
			}
		}
	}

	close(serverSocket);

#ifndef _WIN32
	return NULL;
#endif
}
