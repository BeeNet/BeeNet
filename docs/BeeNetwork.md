# BeeNetwork

* Decentralized/Distributed
* Programming Language: C
* Multithreaded
* Gopher-like default interface
* Better interaction with applications (*not* WebSockets)
* PEX, LDP, and DHT

## Main Distribution Protocol
* Based on BitTorrent ideas
* Download multiple parts/pieces of files from different computers simultaneously (for all files?)
* Diffs for updates? (Myers and Patience diff)

